export default {

  init() {
    console.log('init maps');
    const script = document.createElement('script');
    window.mapUtils = this;
    script.setAttribute('src', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyCDUwOjYVaxUnw2IrLN7k8FSIGU5EWjxvE&callback=mapUtils.processMaps&libraries=&v=weekly');
    document.head.appendChild(script);
  },

  processMaps() {
    document.querySelectorAll('.map-container').forEach(mapContainer => {
      let mapInfo;
      try {
        mapInfo = JSON.parse(mapContainer.dataset.mapinfo);
      } catch (e) {
        console.warn('Invalid map JSON');
        console.error(mapContainer.dataset.mapinfo);
      } finally {
        if(mapInfo) {
          window.mapUtils.processMap(mapContainer, mapInfo)
        }
      }
    });
  },

  processMap(mapContainer, mapInfo) {
    const mapMarker = {
            url: `${mapInfo.path}/marker.svg`,
            anchor: new google.maps.Point(24, 24)
          },
          zoomControlslDiv = document.createElement('div');
    let   map, markers;
    
    switch (mapInfo.type) {
      case 'complete': 
      case 'single':
        [map, markers] = window.mapUtils.processSingleMap(mapContainer, mapInfo, mapMarker);
        break;
      case 'multiple':
        [map, markers] = window.mapUtils.processMultipleMap(mapContainer, mapInfo, mapMarker);
    }

    window.mapUtils.styleZoomControls(zoomControlslDiv, map);

    if(mapInfo[0] == 'multiple') {
      map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(zoomControlslDiv);
    }
  },

  processSingleMap(mapContainer, mapInfo, mapMarker) {
    const location = mapInfo.locations[0],
          latLng = {
            lat: parseFloat(location.latitude),
            lng: parseFloat(location.longitude)
          },
          mapParams = {
            zoom: location.zoom || 16,
            center: latLng,
            mapId: '9a7c4f083fb64779',
            disableDefaultUI: true,      
          },
          map = new google.maps.Map(mapContainer, mapParams),
          markers = [
            new google.maps.Marker({
              position: latLng,
              map: map,
              icon: mapMarker,
            })
          ];

    mapContainer.mapObject = map;

    google.maps.event.addListenerOnce(map, 'tilesloaded', function() {
      mapContainer.classList.add('loaded')
      window.mapUtils.resetMapBounds();
    });  

    return [map, markers];
  },

  processMultipleMap(mapContainer, mapInfo, mapMarker) {
    const locations = mapInfo.locations,
          bounds = new google.maps.LatLngBounds(),
          labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
          coords = locations.map(location => ({
            lat: parseFloat(location.latitude),
            lng: parseFloat(location.longitude)    
          })),
          map = new google.maps.Map(mapContainer, {
            zoom: locations[0].zoom || 16,
            center: coords[0],
            mapId: '9a7c4f083fb64779',
            disableDefaultUI: true,
          });

    mapContainer.mapObject = map;

    const markers = coords.map((coord, i) => {
      const marker = new google.maps.Marker({
        position: coord,
        label: labels[i % labels.length],
        icon: mapMarker,
      });

      bounds.extend(marker.position);

      google.maps.event.addListener(marker, 'click', () => {
        const card = mapContainer.parentElement.querySelector('div.data'),
              button = card.querySelector('a.btn');
        [...card.children].forEach(child => child.classList.add('hidden'));
        for(const property in locations[i]) {
          const label = card.querySelector(`[data-handle="${property}"]`);
          if(label) {
            label.innerHTML = locations[i][property];
            label.classList.remove('hidden');
          }
        }
        if(button) {
          if(locations[i].country) {
            button.dataset.actionParam = button.dataset.apPrefix + locations[i].country;
            button.classList.remove('hidden');
          } else {
            button.classList.add('hidden');
          }
        }
      });

      return marker;
    });

    map.fitBounds(bounds);
    mapContainer.mapBounds = bounds;

    google.maps.event.addListenerOnce(map, 'tilesloaded', function() {
      mapContainer.classList.add('loaded')
      window.mapUtils.resetMapBounds();
    });  

    // Add a marker clusterer to manage the markers.
    const markerClusterer = new MarkerClusterer(map, markers);

    // Change styles after cluster is created
    const styles = markerClusterer.getStyles();
    for (let i = 0; i < styles.length; i++) {
      styles[i].textColor = 'white';
      styles[i].textSize = 14;
      styles[i].url = `${mapInfo.path}/markerclusther.svg`;
    }

    return [map, markers];
  },

  resetMapBounds() {
    document.querySelectorAll('.map-container').forEach(mapContainer => {
      if(mapContainer.mapBounds) {
        mapContainer.mapObject.fitBounds(mapContainer.mapBounds);
      }
    });
  },

  styleZoomControls(zoomControlslDiv, map) {
    zoomControlslDiv.classList.add('zoom-control');
    zoomControlslDiv.style.display = "flex";

    // Set CSS for the control border.
    const zoomout = document.createElement("div");

    zoomout.style.backgroundColor = "#000000";
    zoomout.style.cursor = "pointer";
    zoomout.style.textAlign = "center";
    zoomout.title = "REDUCIR";
    zoomControlslDiv.appendChild(zoomout);

    // Set CSS for the control interior.
    const zoomoutText = document.createElement("div");

    zoomoutText.style.color = "#ffffff";
    zoomoutText.style.fontFamily = "Euclid Circular A,Roboto,Arial,sans-serif";
    zoomoutText.style.fontSize = "12px";
    zoomoutText.style.lineHeight = "16,8px";
    zoomoutText.style.paddingLeft = "32px";
    zoomoutText.style.paddingRight = "12px";
    zoomoutText.style.paddingTop = "18.5px";
    zoomoutText.style.paddingBottom = "18.5px";
    zoomoutText.innerHTML = "REDUCIR";
    zoomout.appendChild(zoomoutText);
    // Setup the click event listeners
    zoomout.addEventListener("click", () => {
      map.setZoom(map.getZoom() - 1);
    });

    // Set CSS for the control border.
    const zoomin = document.createElement("div");

    zoomin.style.backgroundColor = "#000000";
    zoomin.style.cursor = "pointer";
    zoomin.style.textAlign = "center";
    zoomin.title = "AMPLIAR";
    zoomControlslDiv.appendChild(zoomin);

    // Set CSS for the control interior.
    const zoominText = document.createElement("div");

    zoominText.style.color = "#ffffff";
    zoominText.style.fontFamily = "Euclid Circular A,Roboto,Arial,sans-serif";
    zoominText.style.fontSize = "12px";
    zoominText.style.lineHeight = "16,8px";
    zoominText.style.paddingLeft = "12px";
    zoominText.style.paddingRight = "32px";
    zoominText.style.paddingTop = "18.5px";
    zoominText.style.paddingBottom = "18.5px";
    zoominText.innerHTML = "AMPLIAR";
    zoomin.appendChild(zoominText);
    // Setup the click event listeners
    zoomin.addEventListener("click", () => {
      map.setZoom(map.getZoom() + 1);
    });
  }
}