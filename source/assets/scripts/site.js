//-----------------------------------------------------------------------------

const Site = {

  init() {
    
    Dev.init();
    Site.Transitions.init();

    Site.DataAutosize.init();
    Site.DataFocus.init();
    Site.DragToScroll.init();
    Site.Hero.init(_ => Site.Dots.init());
    Site.InputMethod.watchForHover();
    Site.Favicon.init();
    Site.Map.init();
    Site.Nav.init();
    Site.Dropdown.init();
    Site.ImageShadow.init();
    Site.Pagination.init();
    Site.Parallax.init();
    Site.Select.init();
    Site.TextScrolling.init();
    Site.Theming.init();
    Site.Video.init();
    Site.Grid.init();

    // Resize forces scroll
    ['resize', 'orientationchange'].forEach(eventName => {
      window.addEventListener(
        eventName,
        _ => window.dispatchEvent(new CustomEvent('scroll'))
      );
    });

    setTimeout(_ => document.documentElement.classList.add('htmlReady'));

    setTimeout(_ => window.dispatchEvent(new CustomEvent('scroll')), 1000);
  },


  Favicon: {

    init() {
      // Dark mode favicon
      if (window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches) {
        document.querySelectorAll('link[rel="icon"], link[rel="shortcut icon"]').forEach(icon => {
          const href = icon.getAttribute('href');
          icon.setAttribute('href', href.replace('.png', '-negative.png').replace('.ico', '-negative.ico'));
        });
      }
    }
  },


  Nav: {

    init() {
      Site.Nav.manageScrollEvent();
      Site.Nav.manageMenuToggle();
    },

    manageScrollEvent() {
      // Manage scroll-based state
      window.addEventListener('scroll', () =>  {
        const $header = document.querySelector('#header'),
              $html = document.documentElement,
              headerHeight = $header.clientHeight,
              scrollTolerance = 50;

        // Ignore if navigation is expanded
        if($html.classList.contains('navExpanded')) {
          return;
        }

        // Set header height CSS var
        Site.Util.setCssVar('header-height', `${headerHeight}px`);

        // Scrolled past header?
        Site.Nav.scrolledPastHeader = scrollY > headerHeight + scrollTolerance;

        // Last scroll down
        if(scrollY > Site.Nav.lastScrollPosition) {
          Site.Nav.lastScrollDownPosition = scrollY;
        }

        // Scrolling up?
        Site.Nav.scrollingUp = scrollY < Site.Nav.lastScrollDownPosition - scrollTolerance;

        // Scrolled to top?
        Site.Nav.scrolledToTop = scrollY <= 0;

        // Last scroll
        Site.Nav.lastScrollPosition = scrollY;

        // Fixed nav?
        Site.Nav.isFixed =
          !Site.Nav.scrollingUp && Site.Nav.scrolledPastHeader ||
          Site.Nav.scrollingUp && !Site.Nav.scrolledToTop;

        // Visible nav?
        Site.Nav.isVisible = !Site.Nav.scrollUpDisabled && Site.Nav.scrollingUp && !Site.Nav.scrolledToTop;

        // Override fixed/visible if never scrolled past header
        if(Site.Nav.lastScrollDownPosition <= headerHeight + scrollTolerance) {
          Site.Nav.isFixed = false;
          Site.Nav.isVisible = false;
        }

        // Apply classes
        $html.classList.toggle('scrolledToTop', Site.Nav.scrolledToTop);
        $html.classList.toggle('navFixed', Site.Nav.isFixed);
        $html.classList.toggle('navVisible', Site.Nav.isVisible);
      });

      window.dispatchEvent(new CustomEvent('scroll'));
    },

    manageMenuToggle() {
      document.querySelector('.menu-toggle').addEventListener('click', _ => {
        document.documentElement.classList.toggle('navExpanded');
        window.dispatchEvent(new CustomEvent('scroll'));
      });
    },

    disableScrollUp(timeout) {
      Site.Nav.scrollUpDisabled = true;
      setTimeout(_ => Site.Nav.scrollUpDisabled = false, timeout);
    }
  },


  DragToScroll: {

    init() {
      const targets = document.querySelectorAll('.drag-to-scroll');

      targets.forEach(target => {
        target.addEventListener('mousedown', ev =>  {
          window.__isDragging = true;
          target.__scrollDiff = ev.pageX + target.scrollLeft;
        });

        document.documentElement.addEventListener('mousemove', ev => {
          if(window.__isDragging) {
            target.scrollLeft = target.__scrollDiff - ev.pageX;
            ev.preventDefault();
          }
        });

        document.documentElement.addEventListener('mouseup', ev => {
          if(window.__isDragging) {
            window.__isDragging = false;
            ev.preventDefault();
          }
        });
      })
    }
  },


  Util: {

    setCssVar(name, value, element) {
      const root = document.documentElement;
      (element || root).style.setProperty(`--${name}`, value);
    },
  
    getCssVar(name, element) {
      const root = document.documentElement;
      return getComputedStyle(element || root).getPropertyValue(`--${name}`);
    },

    computeAutoMaxHeight(element) {
      const naturalHeight = element.scrollHeight;
      element.style.maxHeight = `${naturalHeight}px`;
    },

    camelToKebab(str) {
      if (str != str.toLowerCase()) {
        str = str.replace(/[A-Z]/g, m => "-" + m.toLowerCase());
        if(/^-/.test(str)) {
          str = str.substring(1);
        }
      }
      return str;
    },

    uncapitalize(str) {
      if(str.length) {
        return str[0].toLowerCase() + str.substring(1);
      }
    },

    rgba(color, alpha) {
      if(/^#/.test(color)) {
        color = color.substring(1);
      }

      const hex = [color.substring(0, 2), color.substring(2, 4), color.substring(4, 6)],
            dec = hex.map(code => parseInt(`0x${code}`));

      return `rgba(${dec[0]},${dec[1]},${dec[2]}, ${alpha})`;
    }
  },


  InputMethod: {

    watchForHover() {
      let hasHoverClass = false,
          lastTouchTime = 0;
    
      function enableHover() {
        // Filter emulated events coming from touch events
        if(
          new Date() - lastTouchTime < 500 ||
          hasHoverClass
        ) {
          return;
        }
    
        document.documentElement.classList.add('hasHover');
        hasHoverClass = true;
      }
    
      function disableHover() {
        if(!hasHoverClass) {
          return;
        }
    
        document.documentElement.classList.remove('hasHover');
        hasHoverClass = false;
      }
    
      function updateLastTouchTime() {
        lastTouchTime = new Date();
      }
    
      document.addEventListener('touchstart', updateLastTouchTime, true);
      document.addEventListener('touchstart', disableHover, true);
      document.addEventListener('mousemove', enableHover, true);
    }
  },


  Dropdown: {

    init() {
      const listHeaders = document.querySelectorAll('.dropdown .list-header'),
            listContainers = document.querySelectorAll('.dropdown .list-container'),
            dropdowns = document.querySelectorAll('.dropdown');

      // Header events
      listHeaders.forEach(listHeader => {
        const dropdown = listHeader.closest('.dropdown');

        // Add icon
        const icon = document.createElement('i');
        icon.classList.add('ri-arrow-down-s-line', 'tt');
        listHeader.appendChild(icon);

        // Toggle on click
        listHeader.addEventListener('click', event => {
          if(dropdown.classList.contains('dropdown--active')) {
            dropdown.classList.toggle('dropdown--expanded');
            event.preventDefault();
          }
        });
      });

      // Manage active class
      dropdowns.forEach(dropdown => {
        if(dropdown.classList.contains('dropdown--sm')) {
          window.addEventListener('resize', Site.Dropdown.onResize(dropdown));
          window.addEventListener('orientationchange', Site.Dropdown.onResize(dropdown));
        } else {
          dropdown.classList.add('dropdown--active');
        }
      });

      // Compute max height
      listContainers.forEach(listContainer => {
        Site.Util.computeAutoMaxHeight(listContainer);
      });

      // Force resize event
      window.dispatchEvent(new CustomEvent('resize'));
    },

    onResize(dropdown) {
      return _ => {
        const isSmartphone = window.matchMedia('(max-width: 767px)').matches;
        dropdown.classList.toggle('dropdown--active', isSmartphone);
      }
    }
  },

  
  ImageShadow: {

    init() {
      document.querySelectorAll('.image-shadow').forEach(elem => {
        const img = elem.querySelector('img'),
              shadow1 = img.cloneNode(true),
              shadow2 = img.cloneNode(true);

        // img.setAttribute('data-parallax', '0,15');
        // shadow2.setAttribute('data-parallax', '0,5');
        elem.appendChild(shadow1);
        elem.appendChild(shadow2);
      });
    }
  },

  
  Parallax: {

    init() {
      window.addEventListener('scroll', () => {
        if (!window.disableParallax) {
          requestAnimationFrame(Site.Parallax.update);
        }
      });
    },

    update() {
      document.querySelectorAll('[data-parallax]').forEach(node => {
        const scrollY = window.scrollY,
          screenHeight = window.innerHeight,
          [transformX, transformY] = node.dataset.parallax.split(','),
          boundingRect = node.getBoundingClientRect(),
          scrollMin = scrollY + boundingRect.top - screenHeight,
          scrollMax = scrollY + boundingRect.bottom,
          t = -.5 + (scrollY - scrollMin) / (scrollMax - scrollMin);
    
        if (t >= -.5 && t <= .5) {
          node.style.transform = `translate3d(${t * transformX}%,${t * transformY}%,0)`;
        }
      });
    }
  },

  TextScrolling: {

    init() {
      document.querySelectorAll('.mod--text-scrolling').forEach(mod => {
        const text = mod.dataset.text.trim(),
              plainText = text.replace('[', '').replace(']', '').replace(/$/, ' '),
              line1 = text.replace('[', '<span>').replace(']', '</span>').replace(/$/, ' '),
              line2 = text.replace('[', '').replace(']', '<span>').replace(/$/, '</span> '),
              p1 = document.createElement('p'),
              p2 = document.createElement('p');

        p1.dataset.parallax = '5,0';
        p2.dataset.parallax = '-5,0';
        p1.innerHTML = plainText.repeat(4) + line1 + plainText.repeat(4);
        p2.innerHTML = plainText.repeat(4) + line2 + plainText.repeat(4);

        mod.appendChild(p1);

        if(!mod.matches('.mod--one-liner')) {
          mod.appendChild(p2);
        }
      });
    }
  },

  Video: {

    init() {
      Site.Video.loadYouTubeAPI();
      window.onYouTubeIframeAPIReady = Site.Video.processEmbeds;
    },

    loadYouTubeAPI() {
      const tag = document.createElement('script'),
            firstScriptTag = document.getElementsByTagName('script')[0];
      tag.setAttribute('src', 'https://www.youtube.com/iframe_api');
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);      
    },

    processEmbeds() {
      document.querySelectorAll('.video-embed').forEach(videoEmbed => {
        Site.Video.loadIframe(videoEmbed);
        videoEmbed.addEventListener('click', () => {
          videoEmbed.classList.add('loading');
          videoEmbed.__player.playVideo();
          setTimeout(_ => videoEmbed.classList.add('ready'), 1000);
        });
      });
    },

    loadIframe(videoEmbed) {
      const ytID = videoEmbed.dataset.youtubeId,
            placeholder = videoEmbed.querySelector('.iframe-placeholder'),
            player = new YT.Player(placeholder, {
              width: '640',
              height: '360',
              videoId: ytID,
              events: {
                onReady: Site.Video.onReady(videoEmbed)
              }
            });
      videoEmbed.__player = player;
    },

    onReady(videoEmbed) {
      return event => {
        videoEmbed.classList.add('loaded');
      }
    }
  },

  Grid: {

    init() {
      document.querySelectorAll('.mod--grid .item').forEach(item => {
        const prefixes = ',xl,lg,md,sm',
              entities = 'Col,Row'
              col = item.dataset.col;

        prefixes.split(',').forEach(prefix => {
          entities.split(',').forEach(entity => {
            const name = Site.Util.uncapitalize(prefix + entity),
                  value = item.dataset[name];
            if (value) {
              Site.Util.setCssVar(Site.Util.camelToKebab(name), value, item);
            }
          })
        });
      });
    }
  },

  Theming: {

    colors: {
      primary:      '#002169',
      mineral:      '#76cdf1',
      orange:       '#ffa253',
      yellow:       '#ffd100',
      grapefruit:   '#e0708c',
      pomegranate:  '#e07066',
    },
    
    init() {
      const selectors = [
              '.mod--cards-product .card',
              '.mod--grid .item',
              '.mod--hero-product'
            ]

      document.querySelectorAll(selectors.join(', ')).forEach(item => {
        const themeElement = item.closest('[data-theme]');

        if (themeElement) {
          const theme = themeElement.dataset.theme,
                isLiteral = /^#/.test(theme),
                color = isLiteral ? theme : Site.Theming.colors[theme];

          // decoration-waves.svg
          item.querySelectorAll('svg.decoration-waves').forEach(waves =>  {
            waves.querySelectorAll('path').forEach((path, i) => {
              let opacity;
              switch(i) {
                case 0:
                  opacity = theme == 'primary' ? .05 : .1;
                  path.setAttribute('fill', color);
                  path.setAttribute('opacity', opacity);
                  break;
                case 1:
                  opacity = theme == 'primary' ? .07 : .15;
                  path.setAttribute('fill', color);
                  path.setAttribute('opacity', opacity);
                  break;
                case 2:
                  opacity = theme == 'primary' ? .1 : .2;
                  path.setAttribute('fill', color);
                  path.setAttribute('opacity', opacity);
                  break;
              }
            });
          });

          // Grid item
          if (item.matches('.item')) {
            const opacity = theme == 'primary' ? .05 : .2;
            item.style.backgroundColor = Site.Util.rgba(color, opacity);
          }

          // decoration-waves-grid.svg
          item.querySelectorAll('svg.decoration-waves-grid').forEach(waves => {
            waves.querySelectorAll('path').forEach((path, i) => {
              const opacity = theme == 'primary' ? .1 : .25;
              switch(i) {
                case 0:
                  path.setAttribute('fill', color);
                  path.setAttribute('opacity', opacity);
                  break;
                case 1:
                  path.setAttribute('fill', color);
                  path.setAttribute('opacity', opacity);
                  break;
              }
            });
          });

          // Product hero
          if (item.matches('.mod--hero')) {
            const imgWrap = item.querySelector('.img-wrap'),
                  decorationSpan = imgWrap.querySelector('span');

            imgWrap.style.backgroundColor = color;
            decorationSpan.innerText = Array(3).fill(decorationSpan.innerText).join(' ');
          }
        }
      });
    }
  },

  Pagination: {

    init() {
      Site.Pagination.general();
      Site.Pagination.cardSlider();
    },

    general() {
      document.querySelectorAll(
        '.mod--map .pagination span, .mod--nav-subsection .pagination span'
      ).forEach(span => {
        span.addEventListener('click', () => {
          const module = span.closest('.mod'),
                slider = module.querySelector('.slider'),
                pageWidth = module.clientWidth,
                itemWidth = module.querySelector('.slider .item').clientWidth,
                gutterWidth = 2 * parseInt(Site.Util.getCssVar('half-gutter')),
                itemsPerPage = Math.floor(pageWidth / itemWidth),
                pageScroll = itemsPerPage * (itemWidth + gutterWidth),
                currentScroll = slider.scrollLeft,
                currentPage = currentScroll / pageScroll,
                direction = span.matches('.prev') ? -1 : 1,
                targetPage = direction < 0 ? Math.floor(currentPage - .01) : Math.ceil(currentPage + .01),
                targetScroll = targetPage * pageScroll;

          slider.scrollTo({ left: targetScroll, behavior: 'smooth' });
        });
      });
    },

    cardSlider() {
      document.querySelectorAll('.mod--cards-slider .pagination span').forEach(span => {
        span.addEventListener('click', () => {
          const module = span.closest('.mod'),
                slider = module.querySelector('.card-container'),
                firstItem = slider.querySelector('.card:first-child'),
                lastItem = slider.querySelector('.card:last-child'),
                direction = span.matches('.prev') ? -1 : 1;

          if (slider.__sliding) {
            slider.__buffer = (slider.__bufffer || 0) + direction;
            return;
          }
          slider.__sliding = true;

          if (direction > 0) {
            slider.classList.add('next');
            setTimeout(_ => {
              slider.classList.add('instant');
              setTimeout(_ => {
                slider.classList.remove('next');
                slider.appendChild(firstItem);
                setTimeout(_ => {
                  slider.classList.remove('instant');
                  Site.Pagination.cardSliderOnFinish(slider);
                }, 50);
              }, 50);
            }, 600);
          } else {
            slider.insertBefore(lastItem, firstItem);
            slider.classList.add('instant', 'next');
            setTimeout(_ => slider.classList.remove('instant', 'next'), 50);
            setTimeout(_ => Site.Pagination.cardSliderOnFinish(slider), 600);
          }
        });
      });
    },

    cardSliderOnFinish(slider) {
      slider.__sliding = false;

      if(slider.__buffer && slider.__buffer != 0) {
        const module = slider.closest('.mod'),
              delta = slider.__buffer > 0 ? -1 : 1,
              span = module.querySelector(`.pagination span.${slider.__buffer > 0 ? 'next' : 'prev'}`);

        slider.__buffer += delta;
        span.dispatchEvent(new CustomEvent('click'));
      }
    }
  },

  DataFocus: {

    init() {
      document.querySelectorAll('[data-focus]').forEach(elem => {
        const focusData = elem.dataset.focus;
        if (focusData) {
          elem.style.objectPosition = focusData;
        }
      });  
    }
  },

  Map: {

    init() {
      if (document.querySelector('.mod--map')) {
        Site.Map.handleDots();
        Site.Map.handleCards();
        Site.Map.handleSelect();
      }
    },

    handleDots() {
      document.querySelectorAll('.mod--map .map-wrap span').forEach(span => {
        const coords = span.dataset.coords.split(',');
        span.style.left = `${coords[0]}%`;
        span.style.top  = `${coords[1]}%`;

        span.addEventListener('click', () => {
          const isSelected = span.matches('.selected'),
                id = span.dataset.id;

          Site.Map.deselect();
          if (!isSelected) {
            Site.Map.select(id)
          }
        });
      });
    },

    handleCards() {
      document.querySelectorAll('.mod--map .slider .item').forEach(item => {
        const id = item.dataset.id,
              dot = document.querySelector(`.mod--map span[data-id="${id}"]`);

        item.addEventListener('mouseenter', _ => {
          dot.classList.add('hover');
        });
        item.addEventListener('mouseleave', _ => {
          dot.classList.remove('hover');
        });
        item.addEventListener('click', event => {
          const isSelected = item.matches('.selected');

          if(isSelected && event.target.closest('.btn')) {
            return;
          }

          Site.Map.deselect();
          if (!isSelected) {
            Site.Map.select(id);
          }
        });
      });
    },

    deselect() {
      document.querySelector('.mod--map').classList.remove('selected');
      document.querySelectorAll('.mod--map, .mod--map .selected').forEach(
        item => item.classList.remove('selected')
      );
    },

    select(id) {
      const item = document.querySelector(`.mod--map .slider [data-id="${id}"]`),
            itemWrap = document.querySelector('.mod--map .item-wrap'),
            itemWrapClone = itemWrap.querySelector('.clone'),
            itemWrapMain = itemWrap.querySelector('.main'),
            dot = document.querySelector(`.mod--map .map-wrap span[data-id="${id}"]`),
            selectWrap = document.querySelector('.mod--map .select-wrap'),
            select = selectWrap.querySelector('select');

      if (item && dot) {
        item.classList.add('selected');
        dot.classList.add('selected');
        document.querySelector('.mod--map').classList.add('selected');
        Site.Nav.disableScrollUp(1000);

        if(window.innerWidth >= 768) {
          item.scrollIntoView({
            block: 'nearest',
            inline: 'center',
            behavior: 'smooth'
          });
        } else {
          dot.scrollIntoView({
            block: 'nearest',
            inline: 'center',
            behavior: 'smooth'
          });
          setTimeout(_ => {
            selectWrap.scrollIntoView({
              block: 'nearest',
              behavior: 'smooth'
            });
          }, 500);
        }

        // Clone first to get height
        itemWrapClone.innerHTML = '';
        itemWrapClone.appendChild(item.cloneNode(true));
        const wrapHeight = itemWrapClone.clientHeight;

        // Transition
        itemWrapMain.classList.add('changing');
        itemWrapMain.style.height = `${wrapHeight}px`;
        setTimeout(_ => {
          itemWrapMain.innerHTML = '';
          itemWrapMain.appendChild(item.cloneNode(true));
          setTimeout(_ => {
            itemWrapMain.classList.remove('changing');
          }, 250)
        }, 250);

        if(select.value != item.dataset.id) {
          select.value = item.dataset.id;
          select.dispatchEvent(new CustomEvent('change'));
        }
      }
    },

    handleSelect() {
      const select = document.querySelector('.mod--map select'), 
            items = document.querySelectorAll('.mod--map .slider .item');

      items.forEach(item => {
        const option = document.createElement('option');
        option.setAttribute('value', item.dataset.id);
        option.innerText = item.querySelector('.title').innerText;
        select.appendChild(option);
      });

      const itemClone = items[0].cloneNode(true);
      document.querySelector('.mod--map .item-wrap .main').appendChild(itemClone);

      select.addEventListener('change', _ => {
        Site.Map.deselect();
        Site.Map.select(select.value);
      });
    }
  },

  Select: {

    init() {
      document.querySelectorAll('.select-wrap select, [data-select-wrap] select').forEach(select => {
        select.addEventListener('update', () => {
          const selectedOption = select.querySelector(':checked'),
                placeholder = select.closest('.select-wrap, [data-select-wrap]').querySelector('.placeholder');
          placeholder.innerText = selectedOption.dataset.label || selectedOption.innerText;
          placeholder.classList.toggle('empty', !selectedOption.value);
        });
        select.addEventListener('change', () => {
          select.dispatchEvent(new CustomEvent('update'));
        });
        select.dispatchEvent(new CustomEvent('update'));
      });
    }
  },

  Dots: {

    init() {
      Site.Dots.inject();
    },
    
    inject() {
      document.querySelectorAll('.dots').forEach(dotsElement => {
        const count = parseInt(dotsElement.dataset.count),
              durations = dotsElement.dataset.duration.split(',').filter(i => parseInt(i));
        
        if (count > 1) {
          for (let i = 0; i < count; i++) {
            // Create and append each item
            const li = document.createElement('li');
            li.dataset.duration = count == durations.length ? durations[i] : durations[0];
            li.dataset.index = i;
            if(i == 0) {
              li.innerHTML = `
                <svg width="40" height="40">
                  <circle cx="20" cy="20" r="19" class="track"></circle>
                  <circle cx="20" cy="20" r="19" class="fill"></circle>
                </svg>`;
            }
            dotsElement.appendChild(li);

            // Click event
            li.addEventListener('click', () => {
              Site.Dots.go(dotsElement, parseInt(li.dataset.index), true);
            });
          }

          // Start sliding
          Site.Dots.go(dotsElement, 0);
        } else {
          dotsElement.parentNode.removeChild(dotsElement);
        }
      });
    },

    go(dotsElement, targetSlide, bypassTransitionend) {
      const li = dotsElement.querySelector(`li:nth-child(${targetSlide + 1})`),
            svg = dotsElement.querySelector('svg'),
            duration = parseInt(li.dataset.duration),
            eventTarget = dotsElement.closest('.dots-target'),
            slideCount = parseInt(dotsElement.dataset.count)

      // Move circle to right position
      dotsElement.dataset.current = targetSlide + 1;

      // Clear timeout in case of manual jump
      clearTimeout(dotsElement.__timeout);

      // Trigger event
      if(eventTarget) {
        eventTarget.dispatchEvent(new CustomEvent('slide', {
          detail: {
            target: targetSlide,
            duration: duration
          }
        }));

        // Set circle animation
        svg.classList.remove('filled');
        Site.Util.setCssVar('transition-duration', `${duration}s`, svg);
        setTimeout(_ => svg.classList.add('filled'));

        // Timeout to next slide
        if (!bypassTransitionend) {
          svg.querySelector('.fill').addEventListener('transitionend', _ => {
            const nextTargetSlide = (targetSlide + 1) % slideCount;
            Site.Dots.go(dotsElement, nextTargetSlide);
          }, { once: true });
        }
      }
    }
  },

  Hero: {

    init(callback) {
      document.querySelectorAll('.mod--hero-home, .mod--hero-section, .mod--hero-subsection').forEach(homeSlider => {
        const hero = homeSlider.closest('.mod--hero');
        homeSlider.addEventListener('slide', Site.Hero.onSlideEvent.bind(hero));
        homeSlider.__outTimeouts = [];
      });

      if(callback) {
        callback();
      }
    },

    onSlideEvent(event) { 
      const hero = this,
            images = hero.querySelectorAll('.img-wrap img'),
            slides = hero.querySelectorAll('.slide');

      images[event.detail.target].classList.remove('out');
      slides[event.detail.target].classList.remove('out');

      clearTimeout(hero.__outTimeouts[0]);
      clearTimeout(hero.__outTimeouts[1]);

      setTimeout(_ => {
        [images, slides].forEach((collection, i) => {
          collection.forEach((item, j) => {
            if (item.matches('.current')) {
              item.classList.add('out');
              hero.__outTimeouts[i] = setTimeout(_ => item.classList.remove('out'), 600);
            }
            item.classList.toggle('current', j == event.detail.target);
          });
        });
      }, 100);
    }
  },

  DataAutosize: {

    init() {
      document.querySelectorAll('[data-autosize]').forEach(elem => {
        switch(elem.dataset.autosize) {
          case 'height-max-child':
            window.addEventListener('resize', Site.DataAutosize.heightMaxChild.bind(elem));
            window.addEventListener('orientationchange', Site.DataAutosize.heightMaxChild.bind(elem));
            break;
        }
      });
    },

    heightMaxChild() {
      let maxHeight = 0;
      const children = [...this.children].forEach(child => {
        maxHeight = Math.max(maxHeight, child.scrollHeight);
      });
      this.style.height = `${maxHeight}px`;
    }
  },

  Transitions: {

    init() {
      const moduleArray = [...document.querySelectorAll('.mod')];

      window.addEventListener('scroll', _ => {
        moduleArray.forEach((mod, index) => {
          const bbox = mod.getBoundingClientRect();

          if(bbox.top < window.innerHeight) {
            mod.classList.add('shown');
            moduleArray.splice(index, 1);
          }
        })
      })
    }
  }
}

//-----------------------------------------------------------------------------

const Dev = {

  init() {
    if(!/solandecabras.es/.test(document.location.href)) {
      // Inject grid
      Dev.Grid.inject();

      // Prevent default for all # links
      document.querySelectorAll('a[href="#"]').forEach(
        a => a.addEventListener('click', event => event.preventDefault())
      );  
    }
  },

  Grid: {

    inject() {
      const devGridHtml = `
              <div id="dev-grid" class="hidden">
                <div class="container container--wide">
                  <div class="container c12">
                    <div class="row">
                      <div class="col-1"><div class="inner"></div></div>
                      <div class="col-1"><div class="inner"></div></div>
                      <div class="col-1"><div class="inner"></div></div>
                      <div class="col-1"><div class="inner"></div></div>
                      <div class="col-1"><div class="inner"></div></div>
                      <div class="col-1"><div class="inner"></div></div>
                      <div class="col-1"><div class="inner"></div></div>
                      <div class="col-1"><div class="inner"></div></div>
                      <div class="col-1"><div class="inner"></div></div>
                      <div class="col-1"><div class="inner"></div></div>
                      <div class="col-1"><div class="inner"></div></div>
                      <div class="col-1"><div class="inner"></div></div>
                    </div>
                  </div>
                  <div class="container c4">
                    <div class="row">
                      <div class="col-3"><div class="inner"></div></div>
                      <div class="col-3"><div class="inner"></div></div>
                      <div class="col-3"><div class="inner"></div></div>
                      <div class="col-3"><div class="inner"></div></div>
                    </div>
                  </div>
                </div>
              </div>
            `;

      // Add grid to DOM
      const devGridElement = (new DOMParser()).parseFromString(devGridHtml, 'text/html').body.childNodes[0];
      document.body.appendChild(devGridElement);

      // Show/hide grid on Ctrl+G
      document.addEventListener('keypress', event => {
        if(event.code == 'KeyG' && event.ctrlKey) {
          const gridIsVisible = !document.querySelector('#dev-grid').classList.toggle('hidden');
          localStorage.setItem('gridIsVisible', gridIsVisible);
        }
      });

      // Show on load?      
      if(localStorage.gridIsVisible && JSON.parse(localStorage.gridIsVisible)) {
        document.querySelector('#dev-grid').classList.remove('hidden')
      }
    }
  }
}

//-----------------------------------------------------------------------------

window.addEventListener('DOMContentLoaded', () => Site.init());